<?php

use PHPUnit\Framework\TestCase;

class FlashCardsTest extends TestCase {

    public function test_getting_available_word(){     
        require "words.php";
        $this->assertStringStartsWith('[gotrandom]|', manage_words('getrandom', 0, 'items_available'));
    }

    public function test_getting_available_word_but_all_is_done(){
        $this->assertStringStartsWith('DONE', manage_words('getrandom', 0, 'all_done'));
    }

    public function test_item_has_been_learnt(){
        $this->assertStringStartsWith('words_updated', manage_words('learnt', 1, 'item_learnt'));
        $this->assertFileEquals('test_words_item_learnt_saved_pattern.json', 'test_words_item_learnt_saved.json');
    }

    public function test_item_has_been_started_over(){
        $this->assertStringStartsWith('startover', manage_words('startover', 1, 'startover'));
        $this->assertFileEquals('test_words_startover_saved_pattern.json', 'test_words_startover_saved.json');
    }

    
}

?>