var word_now
var finished=false

$('#en_word').hide()

getrandom()
function getrandom(){
    $.post( "words.php?op=getrandom", {
    }, function ( data, status ) {
            console.log( 'getrandom', data, status );
        if (data.indexOf('DONE')>-1) {
            finished=true
            $('#nl_word,#en_word').html("WELL DONE!<br>Click here to start over.")

        } else if (data.indexOf('[gotrandom]')>-1) {
            word_now=data.split('|')
            $('#word').attr('wordid',word_now[1])
            $('#nl_word').html(word_now[2])
            $('#en_word').html(word_now[3])
        }
    });		
}

function show(){
    if (finished) {
        $.post( 'words.php?op=startover', {
        }, function ( data, status ) {
            console.log( 'startover ', data, status );
            getrandom()
            finished=false
        });		
    } else {
        $('#word >span').toggle()
    }
}

function ok(){
    if (!finished) {
        $.post( 'words.php?op=learnt&id='+word_now[1], {
        }, function ( data, status ) {
            console.log( 'learnt ', data, status );
            getrandom()
        });	
    }	
}

function bad(){
    if (!finished) getrandom()
}
