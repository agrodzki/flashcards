<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Flash cards</title>
	<script src="js/jquery-3.3.1.min.js"></script>
	<link rel="stylesheet" href="css/milligram.css" type="text/css"/>
</head>
<body>

<div class="outer">  
  <div class="middle">
    <div class="inner">
	<div style="width: 100%; text-align: center; padding-bottom: 11px;">
		<button id="word" onclick="show()" style="width:100%; min-height: 12rem;">
			<span id="nl_word"></span>
			<span id="en_word"></span>
		</button>
	</div>
	<div style="width: 100%; text-align: center; padding-bottom: 11px;">
		<button id="bad" onclick="bad(this)" class="red">👎</button>
		<button id="ok" onclick="ok(this)" class="green">👍</button> &nbsp; &nbsp; 
	</div>
    </div>
  </div>
</div>
<script src="js/words.js"></script>
</body>
</html>