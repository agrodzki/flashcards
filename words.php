<?php	

echo manage_words($_GET['op'], $_GET['id']*1, 0);

function manage_words($op, $id, $test=''){

	if ($test=='') {
		$file_open="words.json";
		$file_save="words.json";
	} else if ($test=='all_done') {
		$file_open="test_words_".$test.".json";
		$file_save="test_words_".$test."_saved.json";
	} else if ($test=='items_available') {
		$file_open="test_words_".$test.".json";
		$file_save="test_words_".$test."_saved.json";
	} else if ($test=='item_learnt') {
		$file_open="test_words_".$test.".json";
		$file_save="test_words_".$test."_saved.json";
	} else if ($test=='startover') {
		$file_open="test_words_".$test.".json";
		$file_save="test_words_".$test."_saved.json";
	}
	

	if ($op=='getrandom') {
		
	  $string = file_get_contents($file_open);
	  $json = json_decode($string);
		shuffle($json);
		$found=false;
		foreach ($json as $key=>$val) {
			if ($val->ok==0) {
				return "[gotrandom]|".$val->id."|".$val->nl."|".$val->en."|".$val->ok;
				$found=true;
				break;
			}
		}
		return $found?'':'DONE';
				
	} else if ($op=='startover') {
		
		$string = file_get_contents($file_open);
		$json = json_decode($string);
	
		$words=array();
	
		foreach ($json as $key=>$val) {
			$val->ok=0;
			$words[]=$val;
		}
	
		$fp = fopen(dirname(__FILE__).'/'.$file_save, 'w');
		fwrite($fp, json_encode($words));
		fclose($fp);

		return 'startover';
		
	} else if ($op=='learnt') {
	
		$string = file_get_contents($file_open);
		$json = json_decode($string);
	
		$words=array();
	
		foreach ($json as $key=>$val) {
			if ($key==$id) $val->ok=1;
			$words[]=$val;
		}
	
		$fp = fopen(dirname(__FILE__).'/'.$file_save, 'w');
		fwrite($fp, json_encode($words));
		fclose($fp);
		
		return 'words_updated';
	}
}


